//
//  LittleMusician.swift
//  MusicianClass
//
//  Created by Rumeysa Bulut on 4.12.2019.
//  Copyright © 2019 Rumeysa Bulut. All rights reserved.
//

import Foundation

class LittleMusician : Musicians {
    func sing2() {
        print("A new song")
    }
    
    override func sing() {
        super.sing() // super is Musicians class
        print("Hello from outsideee")
    }
}
