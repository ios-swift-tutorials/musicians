//
//  main.swift
//  MusicianClass
//
//  Created by Rumeysa Bulut on 1.12.2019.
//  Copyright © 2019 Rumeysa Bulut. All rights reserved.
//

import Foundation

let james = Musicians(nameInit: "James", ageInit: 50, instrumentInit: "Guitar", typeInit: .LeadGuitar)

print(james.type)
james.sing()

let kirk = LittleMusician(nameInit: "Kirk", ageInit: 50, instrumentInit: "Drummer", typeInit: .Drummer)
kirk.sing()
kirk.sing2()
