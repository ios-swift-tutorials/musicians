//
//  Musician.swift
//  MusicianClass
//
//  Created by Rumeysa Bulut on 1.12.2019.
//  Copyright © 2019 Rumeysa Bulut. All rights reserved.
//

import Foundation

enum MusicianType {
    case LeadGuitar
    case Vocalist
    case Drummer
    case Violenist
}

class Musicians {
    var name : String = ""
    var age : Int = 0
    var instrument : String = ""
    var type : MusicianType
    // Initializer ~ Constructor
    init(nameInit: String, ageInit: Int, instrumentInit: String, typeInit: MusicianType) {
        print("Musician created")
        
        name = nameInit
        age = ageInit
        instrument = instrumentInit
        type = typeInit
    }
    
    func sing() {
        print("snap out of it")
    }
    
//   private func sing() {
//        print("snap out of it")
//    }
//  Bu şekilde olsaydı Musicians class'ın objesi olan james bile sing metodunu çağıramazdı.
}
